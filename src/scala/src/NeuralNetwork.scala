import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.{monotonicallyIncreasingId, rand, split}

val sc = new SparkContext(new SparkConf().setAppName("Spark Count"))
val sqlContext = new SQLContext(sc);
import sqlContext.implicits._

import org.apache.spark.sql.SparkSession
val spark = new SparkSession(sc)

// == PROGRAM START ==

/* == CELL 0 == */
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SQLContext}

// Acquire data
var stockData = Map[String, String]();

def getStockHistory(stock: String): String = {
  if (stockData.get(stock) == None) {
    println("Obtaining data for stock " + stock);
    val data = scala.io.Source.fromURL("https://finance.google.com/finance/getprices?i=60&p=15d&f=d,c,v&q=" + stock).mkString
    stockData += (stock -> data);
  }

  return stockData.get(stock).get
}

/* == CELL 1 == */

def isBuggyRecord(arr: Array[Double]): Boolean = {
  for (i <- (0 to arr.length - 2)) {
    if (arr(i+1) - arr(i) <= 0) return true
  }

  return false
}

val inputSize = 200
val predictionWindow = 20

def getPreparedInput(stock: String): DataFrame = {
  val stockHistory = getStockHistory(stock);
  val badPrefixes = List("E", "M", "I", "C", "D", "T", "a")
  val rows = stockHistory.split("\n")
    .filter(_ != "")
    .filter(row => !badPrefixes.exists(e => row.matches(s"^$e.*"))) // Array[String]


  val input_filtered = (0 to rows.size - (inputSize + predictionWindow) - 1) // (0, 4599) // let rows.size = 5000, inputSize = 200
    .map(i => (rows.slice(i, i + inputSize), rows.slice(i + inputSize, i + inputSize + predictionWindow).map(s => s.split("\\,")(1).toDouble).max)) // (rows(0, 200), 322)
    .map(i => (i._2, i._1.map(s => s.split("\\,").map(i => i.toDouble)))) // (322, rows(0, 200) in Array[Array[Double]] form
    .map(i => (i._1, i._2.map(a => a(1)).max, i._2)) // (max of next window, max of current window, rows)
    .filter(i => !isBuggyRecord(i._3.map(j => j(0))))

  // do feature scaling
  val input_scaled = input_filtered
    .map(i => {
      val predictionValue = i._1
      val maxPriceOfCurrentWindow = i._2
      val rows = i._3

      val minPriceValue = Math.min(rows.map(a => a(1)).min, predictionValue)
      val maxPriceValue = Math.max(rows.map(a => a(1)).max, predictionValue)

      val minVolumeValue = rows.map(a => a(2)).min
      val maxVolumeValue = rows.map(a => a(2)).max

      val scaledRows = rows.map(row => {
        val hour = row(0)
        val price = (row(1) - minPriceValue) / (maxPriceValue - minPriceValue)
        val volume = (row(2) - minVolumeValue) / (maxVolumeValue - minVolumeValue)

        Array(hour, price, volume)
      })

      val scaledPredictionValue = (predictionValue - minPriceValue) / (maxPriceValue - minPriceValue)
      val scaledMaxPriceOfCurrentWindow= (maxPriceOfCurrentWindow - minPriceValue) / (maxPriceValue - minPriceValue)

      (scaledPredictionValue, scaledMaxPriceOfCurrentWindow, scaledRows)
    })


  val input = input_scaled
    .map(i => (i._1, i._2, i._3.flatten))
    .map(i => (i._1, i._2, Vectors.dense(i._3)))
    .map(i => (if (i._1 >= i._2) 1 else 0, i._3))

  return sc.parallelize(input).toDF()
    .select($"_1".as("label"), $"_2".as("features"))
    .orderBy(rand());
}

val tslaDF = getPreparedInput("TSLA")
val amznDF = getPreparedInput("AMZN")
val googlDF = getPreparedInput("GOOGL")
val fbDF = getPreparedInput("FB")
val aaplDF = getPreparedInput("AAPL")
val babaDF = getPreparedInput("BABA")
val nvdaDF = getPreparedInput("NVDA")




/* == CELL 2 == */
// prepare dataframe
val stocksDF =
   tslaDF
     .union(amznDF)
     .union(googlDF)
     .union(fbDF)
     .union(babaDF)
     .union(nvdaDF)

  .orderBy(rand())
  .randomSplit(Array(0.8, 0.2), seed = 1234L)

val train_full = stocksDF(0)
val test_full = stocksDF(1)

def getSymmetricDataframe(df: DataFrame): DataFrame = {
  val train_positives = df.filter(row => row.getAs[Integer]("label") == 1)
  val train_negatives = df.filter(row => row.getAs[Integer]("label") == 0).limit(train_positives.count().toInt)

  return train_positives.union(train_negatives).orderBy(rand())
}

val train = getSymmetricDataframe(train_full)
val test = getSymmetricDataframe(test_full);

val train_positive_count = train.filter(row => row.getAs[Integer]("label") == 1).count()
val train_negative_count = train.filter(row => row.getAs[Integer]("label") == 0).count()
println("positive count:" + train_positive_count + " | negative count:" + train_negative_count)



/* == CELL 3 == */
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator

val layers = Array[Int](3 * inputSize, 30, 20, 10, 2)

// create the trainer and set its parameters
val trainer = new MultilayerPerceptronClassifier()
  .setLayers(layers)
  .setBlockSize(32)
  .setSeed(1234L)
  .setMaxIter(100)

// train the model
val model = trainer.fit(train)







/* == CELL 4 == */
// test out the model
val evaluator = new MulticlassClassificationEvaluator()
  .setMetricName("accuracy")

val train_result = model.transform(train)
val train_predictionAndLabels = train_result.select("prediction", "label")

// test out the model
val test_result = model.transform(test)
val test_predictionAndLabels = test_result.select("prediction", "label")

// test out new stock
val unknown_result = model.transform(aaplDF)
val unknown_predictionAndLabels = unknown_result.select("prediction", "label")

println("Train set accuracy = " + evaluator.evaluate(train_predictionAndLabels))
println("Test set accuracy = " + evaluator.evaluate(test_predictionAndLabels))
println("Unknown set accuracy = " + evaluator.evaluate(unknown_predictionAndLabels))

// Precision, recall and other metrics

def printMetrics(title: String, predictionAndLabels: DataFrame) {
  println("\n")
  val truePositives = predictionAndLabels.filter(row => row.getAs[Integer]("label") == 1 && row.getAs[Integer]("prediction") == 1).count().toDouble
  val trueNegatives = predictionAndLabels.filter(row => row.getAs[Integer]("label") == 0 && row.getAs[Integer]("prediction") == 0).count().toDouble
  val falsePositives = predictionAndLabels.filter(row => row.getAs[Integer]("label") == 1 && row.getAs[Integer]("prediction") == 0).count().toDouble
  val falseNegatives = predictionAndLabels.filter(row => row.getAs[Integer]("label") == 0 && row.getAs[Integer]("prediction") == 1).count().toDouble

  val positives_precision = truePositives/(truePositives + falsePositives)
  val positives_recall = truePositives/(truePositives + falseNegatives)
  val positives_f1Score = 2.0 / ((1.0/positives_precision) + (1.0/positives_recall))

  val negatives_precision = trueNegatives/(trueNegatives + falseNegatives)
  val negatives_recall = trueNegatives/(trueNegatives + falsePositives)
  val negatives_f1Score = 2.0 / ((1.0/negatives_precision) + (1.0/negatives_recall))

  println(s"$title | positives-precision: " + positives_precision * 100.0 + "% | positives-recall: " + positives_recall * 100.0 + "% | positives-f1Score: " + positives_f1Score * 100.0 + "%")
  println(s"$title | negatives-precision: " + negatives_precision * 100.0 + "% | negatives-recall: " + negatives_recall * 100.0 + "% | negatives-f1Score: " + negatives_f1Score * 100.0 + "%\n")
}

printMetrics("Train", train_predictionAndLabels)
printMetrics("Test", test_predictionAndLabels)
printMetrics("Unknown", unknown_predictionAndLabels)