import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, Dataset, Row, SQLContext}
import org.apache.spark.sql.functions.{monotonicallyIncreasingId, rand, split}

val sc = new SparkContext(new SparkConf().setAppName("Spark Count"))
val sqlContext = new SQLContext(sc);
import sqlContext.implicits._

import org.apache.spark.sql.SparkSession
val spark = new SparkSession(sc)

// COPY - PASTE STARTS HERE

import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql. {Row, Dataset}

val lr = new LogisticRegression()
lr.setMaxIter(100000)
  .setRegParam(0.000001)
  .setProbabilityCol("myProbability")

// Sanjav testing //
case class Record(label: Int, features: Vector)
val file_nopurchase = sc.textFile("/tmp/eider-user/userfile/sanjav/np.csv");
val file_purchase = sc.textFile("/tmp/eider-user/userfile/sanjav/p.csv");

val train_p_limit = 3000
val train_np_limit = 3000
val test_p_limit = 3940 - train_p_limit
val test_np_limit = 200000

val LABEL_PURCHASE: Int = 1
val LABEL_NOPURCHASE: Int = 0

def attributesToVector(attributes: Array[String]): Vector = Vectors.dense(
  attributes(0).trim.toInt, //x
  attributes(1).trim.toInt, //y
  attributes(0).trim.toInt * attributes(0).trim.toInt, // x2
  attributes(1).trim.toInt * attributes(1).trim.toInt, //y2
  attributes(0).trim.toInt * attributes(1).trim.toInt //xy
  //attributes(0).trim.toInt * attributes(0).trim.toInt * attributes(1).trim.toInt, //x2y
  //attributes(0).trim.toInt * attributes(1).trim.toInt * attributes(1).trim.toInt //xy2
)

val df_purchase_original = file_purchase.map(_.split(","))
  .map(attributes => Record(LABEL_PURCHASE, attributesToVector(attributes)))
  .toDF("label", "features")
  .orderBy(rand())
  .filter($"frequency" < 100)
  .withColumn("id",monotonicallyIncreasingId)

val df_nopurchase_original = file_nopurchase.map(_.split(","))
  .map(attributes => Record(LABEL_NOPURCHASE, attributesToVector(attributes)))
  .toDF("label", "features")
  .orderBy(rand())
  .withColumn("id", monotonicallyIncreasingId)

df_purchase_original.createOrReplaceTempView("df_purchase_original")
df_nopurchase_original.createOrReplaceTempView("df_nopurchase_original")

val id_p_2000 = spark.sql("select max(id) from (select * from df_purchase_original order by id limit " + train_p_limit + " )").toDF().collect()(0).getLong(0)
val id_np_2000 = spark.sql("select max(id) from (select * from df_nopurchase_original order by id limit " + train_np_limit + " )").toDF().collect()(0).getLong(0)

var df_purchase_train = spark.sql("SELECT * from df_purchase_original where id <= " + id_p_2000 + " limit " + train_p_limit).orderBy(rand())
var df_nopurchase_train = spark.sql("SELECT * from df_nopurchase_original where id <= " + id_np_2000 + " limit " + train_np_limit).orderBy(rand())
var df_purchase_test = spark.sql("SELECT * from df_purchase_original where id > " + id_p_2000 + " limit " + test_p_limit).orderBy(rand())
var df_nopurchase_test = spark.sql("SELECT * from df_nopurchase_original where id > " + id_np_2000 + " limit " + test_np_limit).orderBy(rand())

val df_train = df_purchase_train
  .drop("id")
  .union(df_nopurchase_train.drop("id"))
  .orderBy(rand())

val df_test = df_purchase_test
  .drop("id")
  .union(df_nopurchase_test.drop("id"))
  .orderBy(rand())

val model = lr.fit(df_train)

println("== Coefficients ==")
println(model.coefficients)
println("model equation:")
val coeff = model.coefficients
println("1/(1 +(e^(-(" +
  s"(${coeff(0)})*x + " +
  s"(${coeff(1)})*y + " +
  s"(${coeff(2)})*x*x + " +
  s"(${coeff(3)})*y*y + " +
  s"(${coeff(4)})*x*y)" +
  ")))")

def test_model(model: LogisticRegressionModel, dataframe: Dataset[Row], title: String): DataFrame = {
  var corrects: Int = 0;
  var purchase_corrects: Int = 0;
  var nopurchase_corrects: Int = 0;

  var incorrects: Int = 0;
  var purchase_incorrects: Int = 0;
  var nopurchase_incorrects: Int = 0;

  val test_results = model.transform(dataframe)
    .select("features", "label", "myProbability", "prediction")

  test_results.collect().foreach { case Row(features: Vector, label: Int, prob: Vector, prediction: Double) =>
    //TODO: how many purchase vs np corrects and incorrects
    if (prediction.toInt == label)  {
      corrects += 1
      if (label == LABEL_PURCHASE) {
        purchase_corrects += 1
      } else {
        nopurchase_corrects += 1
      }
    } else {
      incorrects += 1
      if (label == LABEL_PURCHASE) {
        purchase_incorrects += 1
      } else {
        nopurchase_incorrects += 1
      }
    }

      return test_results
  }

  val accuracy = corrects.toDouble/(corrects.toDouble+incorrects.toDouble)
  val purchase_accuracy = purchase_corrects.toDouble/(purchase_corrects.toDouble+purchase_incorrects.toDouble)
  val nopurchase_accuracy = nopurchase_corrects.toDouble/(nopurchase_corrects.toDouble+nopurchase_incorrects.toDouble)

  println(title + " corrects count: " + corrects)
  println(title + " purchase_corrects count: " + purchase_corrects)
  println(title + " nopurchase_corrects count: " + nopurchase_corrects)
  println(title + " incorrects count: " + incorrects)
  println(title + " purchase_incorrects count: " + purchase_incorrects)
  println(title + " nopurchase_incorrects count: " + nopurchase_incorrects)

  println(title + " overall accuracy: " + accuracy)
  println(title + " purchase accuracy: " + purchase_accuracy)
  println(title + " nopurchase accuracy: " + nopurchase_accuracy)
}

println("== Train data ==")
test_model(model, df_train, "Train")
println("== Test data ==")
test_model(model, df_test, "Test")

val x = spark.read.json("/tmp/eider-user/userfile/sanjav/people.json").as[Record]