package spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.sql.{Dataset, Row, SQLContext}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.functions.rand

object ScalaWordCount {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("Spark Count"))
    val sqlContext = new SQLContext(sc);
    import sqlContext.implicits._

    val threshold = args(1).toInt

    // split each document into words
    val tokenized = sc.textFile(args(0)).flatMap(_.split(" "))

    // count the occurrence of each word
    val wordCounts = tokenized.map((_, 1)).reduceByKey(_ + _)

    // filter out words with less than threshold occurrences
    val filtered = wordCounts.filter(_._2 >= threshold)

    // count characters
    val charCounts = filtered.flatMap(_._1.toCharArray).map((_, 1)).reduceByKey(_ + _)

    System.out.println(charCounts.collect().mkString(", "))

    case class Record(label: Int, features: Vector)
    val file_nopurchase = sc.textFile("/tmp/eider-user/userfile/sanjav/np.csv");
    val file_purchase = sc.textFile("/tmp/eider-user/userfile/sanjav/p.csv");

    var df_purchase = file_purchase.map(_.split(","))
      .map(attributes =>
        Record(1, Vectors.dense(
          attributes(0).trim.toInt,
          attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(0).trim.toInt,
          attributes(1).trim.toInt * attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(1).trim.toInt
        )))
      .toDF("label", "features")

    df_purchase.orderBy(rand())
  }

  def test(): Unit = {
    val sc = new SparkContext(new SparkConf().setAppName("Spark Count"))
    val sqlContext = new SQLContext(sc);
    import sqlContext.implicits._

    import org.apache.spark.sql.SparkSession
    val spark = new SparkSession(sc)

    import org.apache.spark.ml.classification.LogisticRegression
    import org.apache.spark.ml.linalg.{Vector, Vectors}
    import org.apache.spark.ml.param.ParamMap
    import org.apache.spark.sql.Row

    val lr = new LogisticRegression()
    lr.setMaxIter(1000)
      .setRegParam(0.01)
      .setProbabilityCol("myProbability")

    // Sanjav testing //
    case class Record(label: Int, features: Vector)
    val file_nopurchase = sc.textFile("/tmp/eider-user/userfile/sanjav/np.csv");
    val file_purchase = sc.textFile("/tmp/eider-user/userfile/sanjav/p.csv");

    val train_limit = 2000
    val test_limit = 2000
    def toRecord(attributes: Array[String]): Record = Record(1, Vectors.dense(1,2,3,4))

    val df_purchase_original = file_purchase.map(_.split(",")) // purchase-3940-randomized
      .map(attributes =>
        Record(1, Vectors.dense(
          attributes(0).trim.toInt,
          attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(0).trim.toInt,
          attributes(1).trim.toInt * attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(1).trim.toInt
        )))
      .toDF("label", "features")
      .orderBy(rand())

    var df_purchase_limited = df_purchase_original // purchases-limited
    for (i <- 1 to 10) df_purchase_limited = df_purchase_limited.union(df_purchase_original)
    df_purchase_limited = df_purchase_limited.limit(train_limit)

    val df_nopurchase_original = file_nopurchase.map(_.split(",")) // nopurchase-391000-randomized
      .map(attributes =>
        Record(0, Vectors.dense(
          attributes(0).trim.toInt,
          attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(0).trim.toInt,
          attributes(1).trim.toInt * attributes(1).trim.toInt,
          attributes(0).trim.toInt * attributes(1).trim.toInt
        )))
      .toDF("label", "features")
      .orderBy(rand())


    val df_nopurchase_limited = df_nopurchase_original.limit(train_limit).orderBy(rand()) // nopurchase-limited-randomized

    val df_train = df_purchase_limited.union(df_nopurchase_limited).orderBy(rand()) //purchase+nopurchase -randomized
    val model2: LogisticRegressionModel = lr.fit(df_train)

    var df_test_old = spark.createDataFrame(Seq(
      (1.0, Vectors.dense(2, 4, 4, 16, 8)),
      (0.0, Vectors.dense(1, 14, 1, 196, 14)),
      (1.0, Vectors.dense(2, 2, 4, 4, 4)),
      (0.0, Vectors.dense(85, 10, 7225, 100, 850))
    )).toDF("label", "features")


    var corrects = 0;
    var incorrects = 0;

    val test_results = model2.transform(df_test_old)
      .select("features", "label", "myProbability", "prediction")

      test_results
      .collect()
      .foreach { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
        if (prediction.toInt == label)  corrects += 1  else incorrects += 1
        println(s"($features, $label) -> prob=$prob, prediction=$prediction")
      }

    val hello: Dataset[Row]
    test_results.toDF().

    val df_purchase_3000 = df_purchase_original.limit(3000)
    val df_nopurchase_3000 = df_nopurchase_original.limit(3000)

    df_purchase_original.createOrReplaceTempView("df_purchase_original") // 3940
    df_purchase_limited.createOrReplaceTempView("df_purchase_limited") // 2000
    df_nopurchase_3000.createOrReplaceTempView("df_nopurchase_3000") // 3000
    df_nopurchase_limited.createOrReplaceTempView("df_nopurchase_limited") // 2000

    df_purchase_original.createGlobalTempView("df_purchase_original_global")

    spark.sql("asdf").toDF().collect()(0).getInt(0);

    var df_test_purchase_1000 =
      spark.sql("SELECT * from df_purchase_original where not exists (select 1 from df_purchase_limited");
    // var df_test_nopurchase_1000 = df_nopurchase_original.except(df_nopurchase).limit(train_limit)
    // var df_test = df_test_purchase_1000.union(df_test_nopurchase_1000).orderBy(rand())

  }
}